import { FreeAtHome } from '@busch-jaeger/free-at-home';
import * as https from "https";

const freeAtHome = new FreeAtHome();
freeAtHome.activateSignalHandling();

const default_location: string = "Berlin";
const default_apiKey: string = "e5b368da2a41d4fe09c412203a8f8bee"

let city: string = default_location;
let apiKey: string = default_apiKey;

async function main() {
  const virtualWeatherTempHigh = await freeAtHome.createWeatherTemperatureSensorDevice("fhfchitemp", "Forecast Highest Temperature");
  virtualWeatherTempHigh.setAutoKeepAlive(true);

  const virtualWeatherTempLow = await freeAtHome.createWeatherTemperatureSensorDevice("fhfclotemp", "Forecast Lowest Temperature")
  virtualWeatherTempLow.setAutoKeepAlive(true);

  // weather update interval
  const weatherInterval= setInterval(() => {
    if (city == null || city.length === 0) {
      console.log('City not set, unable to retrieve weather');
      return;
    }

    const options = {
      host: 'api.openweathermap.org',
      path: `/data/2.5/weather?q=${encodeURIComponent(city)}&units=metric&appid=${apiKey}`,
      method: 'GET',
      headers: {
        'Accept': 'application/json'
      }
    }

    console.log(options)

    let request = https.request(options, (res) => {
      if (res.statusCode !== 200) {
        console.error(`Could not receive weather information. Error code: ${res.statusCode}\nError: ${res.statusMessage}`);
        res.resume();
        return;
      }

      let data = '';

      res.on('data', (chunk) => {
        data += chunk;
      });

      res.on('close', () => {
        const weather = JSON.parse(data);
        virtualWeatherTempLow.setTemperature(weather.main.temp_min);
        virtualWeatherTempHigh.setTemperature(weather.main.temp_max);
      });
    });

    request.end();

    request.on('error', (err) => {
      console.error(`Encountered an error while retrieving weather data: ${err.message}`);
    });
  }, 3600000);

  // Handle shutting down of the add-on
  if (process.platform === "win32") {
    var rl = require("readline").createInterface({
      input: process.stdin,
      output: process.stdout
    });

    rl.on("SIGINT", function () {
      process.emit("SIGINT" as any);
    });
  }

  process.on('SIGINT', async () => {
    console.log("SIGINT received, cleaning up...")
    await freeAtHome.markAllDevicesAsUnresponsive();
    clearInterval(weatherInterval);
    console.log("clean up finished, exiting procces")
    process.exit();
  });
  process.on('SIGTERM', async () => {
    console.log("SIGTERM received, cleaning up...")
    await freeAtHome.markAllDevicesAsUnresponsive();
    console.log("clean up finished, exiting procces")
    process.exit();
  });
}

main();

// Get notified about changes in the configuration of the add on
//#################################################################################

import {AddOn} from '@busch-jaeger/free-at-home';

const metaData = AddOn.readMetaData();

const addOn = new AddOn.AddOn<ConfigurationProperties>(metaData.id);

addOn.on("configurationChanged", (configuration: ConfigurationProperties) => {
  city = configuration["default"]?.items?.["city"] ?? default_location;
  apiKey = configuration["default"]?.items?.["apiKey"] ?? default_apiKey;
});

addOn.connectToConfiguration();

interface ConfigurationProperties extends AddOn.Configuration {
  default: {
    items: {
      city: string,
      apiKey: string
    }
  }
}